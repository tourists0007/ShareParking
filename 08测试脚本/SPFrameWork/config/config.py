"""
读取配置。这里配置文件用的yaml，也可用其他如XML,INI等，需在file_reader中添加相应的Reader进行处理。
"""
import os
from utils.readyaml import YamlReader

# 通过当前文件的绝对路径，其父级目录一定是框架的config目录，然后确定各层的绝对路径。
BASE_PATH = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
CONFIG_FILE = os.path.join(BASE_PATH, 'config', 'config.yml')
DATA_PATH = os.path.join(BASE_PATH, 'data')
DRIVER_PATH = os.path.join(BASE_PATH, 'config', 'base.cnf')
LOG_PATH = os.path.join(BASE_PATH, 'logs')
REPORT_PATH = os.path.join(BASE_PATH, 'report')
TEST_FILE = os.path.join(BASE_PATH, 'config', 'test_info.cnf')
IMAGE_PATH = os.path.join(BASE_PATH, 'docs','image')
CONFIG_PATH = os.path.join(BASE_PATH, 'config')
PAGE_PATH = os.path.join(CONFIG_PATH, 'SPPage')
CASE_PATH = os.path.join(BASE_PATH, 'test', 'cases')
PIC_PATH = os.path.join(BASE_PATH, 'docs', 'pic')

class Config:

    def __init__(self, conf=CONFIG_FILE):
        self.config = YamlReader(conf).data

    def get(self, element, index=0):
        """
        yaml是可以通过'---'分节的。用YamlReader读取返回的是一个list，第一项是默认的节，如果有多个节，可以传入index来获取。
        这样我们其实可以把框架相关的配置放在默认节，其他的关于项目的配置放在其他节中。可以在框架中实现多个项目的测试。
        """
        return self.config[index].get(element)


if __name__ == '__main__':

    pass
    r = Config().get('redis_params')
    # r = c.get('file_name')
    print(r, type(r))
    # r = YamlReader(CONFIG_FILE).data[0].get()
    # print(r,type(r))


