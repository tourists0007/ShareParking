"""
断言模块
"""
import os
from utils.log import Logger
from config.config import PIC_PATH, Config
from utils.dbutil import MysqlUtil
from GUI.keywords import KeyWords as kw
from collections import Iterable

class CustomAssert:
    
    module_name_ = os.path.basename(__file__).split('.')[0]
    logger = Logger.get_logger(module_name_)
    _db_params = eval(Config().get('test_mysql_params'))
    mysqlobj = MysqlUtil(_db_params)

    @classmethod
    def write_result_to_db(cls, version, table, info, result_msg, error_msg, error_img_path):
        """
        将测试结果写入数据库中
        :param version: 当前测试版本
        :param table: 数据表
        :param info: 测试信息的字典形式
        :param result_msg: 结果信息
        :param error_msg: 错误信息
        :param error_img_path: 错误截图路径
        :return: None
        """
        try:
            case_version = version
            case_id = info["case_id"]
            module_name = info["module_name"]
            test_type = info["test_type"]
            api_url = info["api_url"]
            request_method = info["request_method"]
            case_desc = info["case_desc"]
            test_data = info["test_data"]
            expect = info["expect"]

            sql = f'insert into {table}(case_version,case_id,module_name,test_type,api_url,' \
                  f'request_method,case_desc,case_params,expect,result_msg,error_msg,error_img_path) ' \
                  f'values("{case_version}", "{case_id}", "{module_name}", "{test_type}",' \
                  f'"{api_url}", "{request_method}", "{case_desc}", "{test_data}", "{expect}", ' \
                  f'"{result_msg}","{error_msg}",{error_img_path})'
            print(sql)
            if not cls.mysqlobj.update(sql):
                cls.logger.error('sql执行错误')
        except Exception as e:
            cls.logger.error('write_result_to_db参数错误' + e.__str__())

    @classmethod
    def assert_error(cls, version, table, info):
        """
        断言错误处理
        :param version: 版本名称
        :param table: 数据库表
        :param info: 测试信息
        :return: None
        """
        result_msg = 'test error'
        error_img_path = kw().get_screenshot()
        cls.write_result_to_db(version, table, info, result_msg, info['error_msg'] ,error_img_path)

    @classmethod
    def assert_equal(cls, version, actual, info, table):
        """
        断言相等处理
        :param version: 当前版本
        :param actual: 实际结果
        :param info: 测试信息
        :param table: 数据库表
        :return: None
        """
        if info['expect'] == actual:
            result_msg = 'test pass'
            error_msg = '无'
            error_img_path = "'dyz'"
        else:
            result_msg = 'test fail'
            error_msg = "无"
            error_img_path = kw().get_screenshot()

        cls.write_result_to_db(version, table, info, result_msg, error_msg, error_img_path)


    @classmethod
    def assert_contain(cls, version, actual, info, table):
        # if isinstance(actual, Iterable):
        # if actual is not None:
        if actual is not None and info['expect'] in actual:
            result_msg = 'test pass'
            error_msg = '无'
            error_img_path = '"无"'
        else:
            result_msg = 'test fail'
            error_msg = "无"
            error_img_path = kw().get_screenshot()
            print(error_img_path)

        cls.write_result_to_db(version, table, info, result_msg, error_msg, error_img_path)
        return

"""class Assertion:


    def __init__(self):
        pass

    def assert_in(self, actual, expect, msg=None):
        _flag = False
        try:
            assert expect in actual, 'not in'
            result = 'assert success'
            _flag = True
        except:
            result = 'assert fail'
        finally:
            sql = f'UPDATE wb_test_result SET assert="{result}" WHERE tid=1;'
            inssert_assert = MysqlUtil.insertTable(sql)
        return _flag"""

if __name__ == '__main__':
    info_dict = {'expect':'pass1', 'case_id':'test001','module_name':'login','case_desc':'dev','test_data':'dyz',
              'test_type':'功能测试','api_url':'172.16.3.124','request_method':'pass',}
    r = CustomAssert.assert_contain('V1.0','dyz pass',info_dict, 'test')
    print(r)

    pass