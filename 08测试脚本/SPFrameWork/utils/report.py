"""
测试报告模块
"""

import os
from utils.fileutil import FileUtils
from utils.base import TimeUtil
from utils.dbutil import MysqlUtil
from utils.log import Logger
from config.config import REPORT_PATH,Config


class Report:
    module_name_ = os.path.basename(__file__).split('.')[0]
    logger = Logger.get_logger(module_name_)

    @classmethod
    def genarate_test_report(cls, version, table_name, app_name):

        contents = FileUtils(REPORT_PATH+'/template.html').read_all_txt()
        datetime = TimeUtil.get_time().split(' ')
        report_date = datetime[0]
        report_time = datetime[1]
        contents = contents.replace('$test-date', report_date)
        contents = contents.replace('$test-version', version)
        sql1 = f'select count(*) from {table_name} where result_msg="test pass"'
        _db_params = eval(Config().get('test_mysql_params'))
        mysqlutil = MysqlUtil(_db_params)
        pass_count = mysqlutil.db_query(sql1)[0][0]
        # pass_count = MysqlUtil(_db_params).db_query(sql1)
        contents = contents.replace('$pass-count', str(pass_count))
        sql2 = f'select count(*) from {table_name} where result_msg="test fail"'
        fail_count = mysqlutil.db_query(sql2)[0][0]
        contents = contents.replace('$fail-count', str(fail_count))
        sql3 = f'select count(*) from {table_name} where result_msg="test error"'
        error_count = mysqlutil.db_query(sql3)[0][0]
        contents = contents.replace('$error-count', str(error_count))
        contents = contents.replace('$last-time', report_time)

        # 获取某个版本的所有数据
        sql4 = f'select * from {table_name} where case_version="{version}"'
        test_result_data = mysqlutil.db_query(sql4)
        if len(test_result_data) == 0:
            cls.logger.info('该版本没有执行测试')
        else:
            color = ''
            test_result = ''
        # 将从数据库获取的数据遍历写入html的表格中
            for data in test_result_data:
                print(data)
                if data[11] == 'test pass':
                    color = 'green'
                elif data[11] == 'test fail':
                    color = 'blue'
                else:
                    color = 'red'
                test_result += f'<tr height="40">' \
                               f'<td width="7%">{str(data[0])}</td>' \
                               f'<td width="9%">{data[3]}</td>' \
                               f'<td width="9%">{data[4]}</td>' \
                               f'<td width="7%">{data[2]}</td>' \
                               f'<td width="20%">{data[7]}</td>' \
                               f'<td width="7%" bgcolor={color}>{data[11]}</td>' \
                               f'<td width="16%">{str(TimeUtil.get_time())}</td>' \
                               f'<td width="15%">{data[9]}</td>' \
                               f'<td width="10%"><a href={data[12]} target="_blank">查看截图</a></td>' \
                               f'</tr>'
            contents= contents.replace('$test-result', test_result)

            report_name = f'{REPORT_PATH}\\{app_name}{version}版本测试报告_{TimeUtil.get_time()}.html'

            with open(report_name, 'w', encoding='utf8') as file:
                file.write(contents)
            cls.logger.info('报告生成成功')




if __name__ == '__main__':
    Report.genarate_test_report('v1.0', 'test', 'sp')