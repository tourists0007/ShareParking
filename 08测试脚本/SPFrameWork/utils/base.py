'''
基本工具
'''
import os,time
from collections import Iterable


class TimeUtil:

    @classmethod
    def get_time(cls, format='%Y-%m-%d %H-%M-%S'):
        """
        按规定格式返回当前日期时间字符串
        :return:规定格式的日期时间字符串
        """
        import time
        return time.strftime(format, time.localtime())


class CI:
    """
    CI持续集成类，完成拉取代码、构建、部署、测试、发送邮件的CI过程
    """
    # 完成拉取代码、构建、部署、测试、发送邮件的CI过程
    def checkout(self, app_path, git_url):

        if os.path.exists(app_path):
            os.system(f'git -C {app_path} pull origin master')
        else:
            os.system(f'git clone {git_url} {app_path}')

        time.sleep(10)
        print('**********************************')

    def build(self, xml_path):
        os.system(f'ant -f {xml_path}')
        time.sleep(5)
        print('**********************************')

    def deploy(self, linux_password, src_path, target_path):
        os.system(f'pscp -pw {linux_password} {src_path} {target_path}')
        time.sleep(10)
        print('**********************************')

    def test(self):
        from selenium import webdriver
        driver = webdriver.Firefox()
        driver.implicitly_wait(5)
        driver.get('http://172.16.9.30:8080/woniusales/')
        driver.find_element_by_id('username').send_keys('admin')
        driver.find_element_by_id('password').send_keys('Milor123')
        driver.find_element_by_id('verifycode').send_keys('0000')
        driver.find_element_by_css_selector('button.form-control').click()
        time.sleep(5)
        driver.quit()

    def send_email(self):
        pass
class Decorator:

    @classmethod
    def casedata(cls,datadict):
        # try:
        if isinstance(datadict, Iterable):
                def myfun(func):
                    def newfunc(*args):
                        for dd in datadict:
                            func(*args,dd)
                    return newfunc
                return myfun
            # return
        else:
            return None
        # except Exception as e:
        #     return None

class base:

    @staticmethod
    def make_dir(dir_path):
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)



if __name__ == '__main__':
    CI().run()