"""
框架主入口
"""
from utils.report import Report
from utils.suite import Suite

class Run:

    def __init__(self):

        pass

    @classmethod
    def run(cls,*arg):
        try:
            if arg == ():
                for fun in Suite.suite():
                    fun()
        except Exception as e:
            print(e.__str__())
        finally:
            Report.genarate_test_report('V1.0', 'test', 'SP')

Run.run()