# webTest框架介绍

### 简介

    本框架基于Python3+selenium3组成，用户以Page Object的模式编写用例。

    元素的定位和操作按照页面划分，达到Web端自动化回归测试的目的，
    并生成测试报告。浏览器兼容性暂时未完善。

    本例子展示了抢租客修改信息功能的测试用例，比较简陋。

列举一下相关的亮点:

- 💜 采用po模式，定位元素与实际操作分离，同一个页面的操作代码可复用
- 🖤 优化元素定位为隐式等待，拒绝代码中time.sleep等待元素
- 💔 

### 快速上手

- 从远程仓库拉取代码

```git clone https://gitee.com/TestGroup51/ShareParking.git```

- 安装依赖包

进入下载好的SPFrameWork目录，并在此目录运行安装依赖, 需要安装好pip(默认自带的就行)

- 运行demo

在SPFrameWork目录输入命令(确保Firefox浏览器及对应驱动已安装):

```python3 run.py```


#### 配置说明

    见config.py，大部分说明都有对应的描述，
   

### 环境配置

- os: Windows
- Python3.x
- Firefox浏览器


### 目录结构

```
SPFrameWork

└───config
|   |    SPPage
|   |    config.py
|   |    config.yaml
|
└───data
|
└───docs
|   |    image
|
└───GUI
|   |    webdriver
|   |    driver.py
|   |    kewwords.py
|
└───logs
|
└───report
|
└───tests
|   |    case
|   |    page
|
└───util
|   |   assert.py
|   |   base.py
|   |   dbutil.py
|   |   filetil.py
|   |   log.py
|   |   readyaml.py
|   |   report.py
|   |   suite.py
|
└───webapps
│   README.md
│   run.py

```

### 内容介绍

- config

  存放此框架配置文件，包括页面元素定位方式包、数据库配置、浏览器配置等，详细见config.py
  
  - SSPage
  
    SP项目页面目录, 可扩展, 针对不同模块的页面可设计不同目录结构。一般存放该页面的元素定位数据(locator)
    
  - config.py

    存放配置路径、读取配置文件config.yaml方法
  
- data

  存放测试数据，暂时仅支持excel文件格式

- docs

  存放文档，截图及一些临时文件
  
- GUI

  - webdriver
    
    webdriver包，存放各个浏览器驱动（暂未实现）
    
  - driver.py

    对webdriver进行二次封装
  
  - keywords.py

    对webdriver中动作进行二次封装，eg：input、click。。。

- logs

  存放所有日志文件, 目前只有test.log, 主要目的是将日志区分开来。

- report

  存放测试报告，为html形式，可右键通过浏览器打开。

- test

  测试目录, 可扩展, 子目录为测试用例及页面动作。

  - cases

    存放基础测试用例。
    
  - page

    对各个页面动作封装，可根据项目自行扩展，目前有SP项目基础模块（base_page.py）及抢租客模块示例（rob_tenants.py）
  
- util

  - assert.py

    对断言的二次封装，可扩展

  - base.py

    用于存放时间、CI持续集成、装饰器等类, 主要是一些基础的小工具。

  - dbutil.py

    数据库模块，目前对MySQL、redis数据库进行二次封装，可扩展
    
  - fileutil.py

    读取文件模块，包含对excel、json、ini、cnf、yaml格式文件读取及数据解析，可按需求扩展

  - log.py

    记录日志的功能函数。

  - readyaml.py

    读取配置文件config.yml文件的方法

  - report.py

    生成报告模块，生成html报告
  
  - suite.py

    测试套件模块，选择以test开头或结束的用例执行


#### 注: 以上目录结构/命名可能并不合理, 持续优化

---

### 使用手册

##### 以下内容若已安装, 可跳过。

- 安装Python3

  [Python3.6下载地址](https://www.python.org/downloads/release/python-364/)

  下载对应操作系统的Python版本并安装。

- 下载IDE(非必须)

  推荐Pycharm

  [Pycharm下载地址](https://www.jetbrains.com/pycharm/)

- 安装必须的库

  还没有梳理出来。。。

- 用例编写规则

  Python:

  - 可参考test_case_demo.py编写用例(最好用例的类名不重复)
  - 用例需要写在test/cases/目录下
  - 用例需要以test开头如test_bmp
  - 用例的test函数需要带上@Decorator.casedata([{testdata}]),测试数据格式为[{},{},...{}]
  - 日志在/log/test.log查看,每天的日志为一个文件
  - SPPage目录编写项目各个页面需要元素定位方式，格式为cnf，模块以角色划分（rob_tenants.cnf...）
  - /test/page/目录存放对每个页面的动作封装，其中每个类需要继承basepage.py
  - cnf配置文件中字母小写