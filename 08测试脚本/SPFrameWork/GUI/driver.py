import os
from utils.log import Logger
from config.config import Config

'''class UIKeyUtil(UIDriver):

    module_name_ = os.path.basename(__file__).split('.')[0]
    logger = LoggerUtils.get_logger(module_name_)
    # webdriver = None
    #
    #
    # @classmethod
    # def get_driver(cls):
    #     from selenium import webdriver
    #     browser = FileUtils('../config/base.cnf').get_ini_option_value('ui', 'browser')
    #     # 使用单例模式判断driver是否为空对象
    #     if cls.webdriver == None:
    #         try:
    #             cls.webdriver = getattr(webdriver, browser)()
    #             cls.webdriver.implicitly_wait(5)
    #             cls.webdriver.maximize_window()
    #         except Exception as e:
    #             cls.logger.error('浏览器配置信息错误  '+ str(e))
    #             cls.webdriver = None
    #     return cls.webdriver
    def __init__(self):
        self.__driver = UIDriver.get_driver()
        pass
    __driver = UIDriver.get_driver()
    @classmethod
    def open(cls, url):
        try:
            cls.__driver.get(url)

        except Exception as e:
            cls.logger.error(f'driver或{url}异常'+ str(e))

    @classmethod
    def query_element(cls, loc):
        element = None
        try:
            element = cls.__driver.find_element(getattr(By, loc[0]), loc[1])
        except Exception as e:
            cls.logger.error(f'元素定位方式{loc[0]}或{loc[1]}异常'+ str(e))
        return element

    @classmethod
    def input(cls,  loc, value):
        # temp = str(args[0]).split('=')
        # element = cls.query_element(temp[0], temp[1])
        # element.click()
        # element.clear()
        # element.send_keys(args[1])
        element = cls.query_element(loc)
        if element != None:
            element.click()
            try:

                element.send_keys(value)
            except Exception as e:
                cls.logger.error(f'输入文本{value}异常' + str(e))

    # @classmethod
    # def input(cls, element, value):
    #     element.click()
    #     element.clear()
    #     element.send_keys(value)

    @classmethod
    def input_textarea(cls, loc, value):
        """
        向文本域输入内容
        :param loc: 文本域元素定位信息
        :param value: 要输入的值
        :return: None
        """
        element = cls.query_element(loc)
        if element != None:
            element.click()
            element.send_keys(value)

    @classmethod
    def click(cls, loc):
        element = cls.query_element(loc)
        if element != None:
            element.click()

    @classmethod
    def quit(cls):
        cls.__driver.quit()

    @classmethod
    def select_random_option(cls, loc):
        """
        从下拉框中随机选择一个option
        :return:
        """
        import random
        element = cls.query_element(loc)
        if element != None:
            random_index = random.randint(0, len(Select(element).options))
            Select(element).select_by_index(random_index)

    @classmethod
    def select_option(cls, loc, text):
        """
        根据select的option值进行选择
        :param element:
        :param text:
        :return:
        """
        element = cls.query_element(loc)
        if element != None:
            Select(element).select_by_visible_text(text)

    @classmethod
    def find_element_by_attr(cls, element_attr):
        """
        找到元素对象并返回
        :param webdriver: 浏览器驱动对象
        :param element_attr: 元素属性
        :return: 元素对象
        """

        return cls.__driver.find_element(getattr(By, element_attr[0]), element_attr[1])

    @classmethod
    def is_element_present(cls, loc):
        """
                判断某个元素是否存在
                :param webdriver: 浏览器驱动对象
                :param element_attr: 元素的属性的元组形式
                :return: 是否存在的标识
                """
        try:
            element = cls.query_element(loc)
            return True
        except Exception as e:
            return False'''
'''class ImgMatchUtil:

    mouse = PyMouse()
    keyboard = PyKeyboard()
    module_name_ = os.path.basename(__file__).split('.')[0]
    logger = LoggerUtils.get_logger(module_name_)

    @classmethod
    def match_img(cls, target):
        image_path = "..\\image"
        template_path = os.path.join(image_path, target)
        screen_path = os.path.join(image_path, 'screen.png')
        from PIL import ImageGrab
        ImageGrab.grab().save(screen_path)
        import cv2
        # 读取大图
        screen = cv2.imread(screen_path)
        # 读取小图
        template = cv2.imread(template_path)
        # 调用匹配算法
        result = cv2.matchTemplate(screen, template, cv2.TM_CCOEFF_NORMED)

        min, max, min_loc, max_loc = cv2.minMaxLoc(result)

        # 计算矩形十字中心点的坐标
        x = max_loc[0] + int(template.shape[1] / 2)
        y = max_loc[1] + int(template.shape[0] / 2)

        # 如果没匹配成功，则返回的是-1，-1
        return x,y

        # 单击图片元素
    @classmethod
    def click_image(cls, target):
        x, y = cls.match_img(target)
        if x == -1 and y == -1:
            cls.logger.error(f'没有找到{target}图片')
            return
        cls.mouse.click(x, y)

    # 双击图片元素
    @classmethod
    def double_click_image(cls, target):
        x, y = cls.match_img(target)
        if x == -1 and y == -1:
            cls.logger.error(f'没有找到{target}图片')
            return
        cls.mouse.click(x, y, n=2)

    # 向一个文本框图片输入
    @classmethod
    def input_image(cls, target, value):
        x, y = cls.match_img(target)
        if x == -1 and y == -1:
            cls.logger.error(f'没有找到{target}图片')
            return
        cls.keyboard.type_string(value)'''


class Driver:

    module_name_ = os.path.basename(__file__).split('.')[0]
    logger = Logger.get_logger(module_name_)

    driver = None

    @classmethod
    def get_driver(cls):
        from selenium import webdriver
        # browser = FileUtils(DRIVER_PATH).get_ini_option_value('ui', 'browser')
        browser = Config().get('browser')
        # 使用单例模式判断driver是否为空对象
        if cls.driver is None:
            try:
                cls.driver = getattr(webdriver, browser)()
                cls.driver.implicitly_wait(5)
                cls.driver.maximize_window()
            except Exception as e:
                cls.logger.error('浏览器配置信息错误  ' + str(e))
                cls.driver = None
        return cls.driver
