"""
测试用例模块演示
"""
from test.page.rob_tenants import BuyCarnum,RTMy
from test.page.property import Property
from test.page.rob_pt import PT1,PT2,PT3
from test.page.rob_lessor import PT,Rent_out,My
from utils.base import Decorator
from GUI.keywords import KeyWords
from utils.assertutil import CustomAssert as cassert
from utils.fileutil import ParserUtils
from config.config import DATA_PATH
import os

class TestRTMy:


    info_dict_list = ParserUtils(os.path.join(DATA_PATH,'ui_case.xlsx')).parse_test_info_dict('rob_tenants')
    @Decorator.casedata(info_dict_list)
    def test_rob_tenants(self, info_dict):
        print(1)
        test_data = info_dict['test_data']
        RTMy().change_info(**test_data)
        actual = KeyWords().get_ele_txt(RTMy.info_name)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')

    info_dict_list = ParserUtils(os.path.join(DATA_PATH,'ui_case.xlsx')).parse_test_info_dict('rob_pt')
    @Decorator.casedata(info_dict_list)
    def test_rob_pt(self, info_dict):
        print(2)
        test_data = info_dict['test_data']
        PT1().change_info(**test_data)
        actual = KeyWords().get_ele_txt(PT1.info_name)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')


    info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('rob_pt_site')
    @Decorator.casedata(info_dict_list)
    def test_rob_pt_site(self, info_dict):
        print(3)
        test_data = info_dict['test_data']
        PT2().change_info(**test_data)
        actual = KeyWords().get_ele_txt(PT2.info_name)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')

    #
    info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('rob_pt_order_status')
    @Decorator.casedata(info_dict_list)
    def test_rob_pt_order_status(self, info_dict):
        print(4)
        test_data = info_dict['test_data']
        PT3().change_info(**test_data)
        actual = KeyWords().get_ele_txt(PT3.info_name)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')

    # info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('pt')
    # @Decorator.casedata(info_dict_list)
    # def test_increase_lessor(self, info_dict):
    #     print(5)
    #     test_data = info_dict['test_data']
    #     PtUser().insert_lessor(**test_data)
    #     actual = KeyWords().get_ele_txt(PtUser.information)
    #     cassert.assert_contain('v1.0', actual, info_dict, 'test')

    info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('rob_lessor_addcar')
    @Decorator.casedata(info_dict_list)
    def test_rob_lessor_addcar(self, info_dict):
        print(6)
        test_data = info_dict['test_data']
        PT().change_info(**test_data)
        actual = KeyWords().get_ele_txt(PT.info_name)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')

    info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('lessor_my')
    @Decorator.casedata(info_dict_list)
    def test_xuy(self, info_dict):
        print(7)
        test_data = info_dict['test_data']
        My().change_info(**test_data)
        actual = KeyWords().get_ele_txt(My.lessors_a)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')

    info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('lessor')
    @Decorator.casedata(info_dict_list)
    def test_lessor(self, info_dict):
        print(8)
        test_data = info_dict['test_data']
        Rent_out().change_info(**test_data)
        actual = KeyWords().get_ele_txt(Rent_out.mymodallabel_a)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')


    info_dict_list = ParserUtils(os.path.join(DATA_PATH,'ui_case.xlsx')).parse_test_info_dict('change')
    @Decorator.casedata(info_dict_list)
    def test_change_info(self, info_dict):
        print(9)
        test_data = info_dict['test_data']
        Property().change_info(**test_data)
        actual = KeyWords().get_ele_txt(Property.alert_info)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')
    #
    info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('property')
    @Decorator.casedata(info_dict_list)
    def test_add_info(self, info_dict):
        print(10)
        test_data = info_dict['test_data']
        Property().add_pro(**test_data)
        actual = KeyWords().get_ele_txt(Property.alert_info)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')
    #
    info_dict_list = ParserUtils(os.path.join(DATA_PATH, 'ui_case.xlsx')).parse_test_info_dict('order_history')
    @Decorator.casedata(info_dict_list)
    def test_buy(self, info_dict):
        print(11)
        test_data = info_dict['test_data']
        BuyCarnum().buy_carnum()
        BuyCarnum().query_order()
        actual = KeyWords().get_ele_txt(BuyCarnum.info_order)
        cassert.assert_contain('v1.0', actual, info_dict, 'test')


    # def test_zhange_quit(self):
    #     KeyWords().quit()





if __name__ == '__main__':
    # TestRTMy().test_rob_lessor_addcar()


    pass
