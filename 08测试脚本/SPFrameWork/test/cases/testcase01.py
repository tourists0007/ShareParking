from utils.base import Decorator

class Test1:

    @Decorator.casedata([{'a':111}, {'c':112}])
    def test101(self,a):
        print(f'tc0101,{a}')

    @Decorator.casedata([{'b': 222}])
    def test102(self,b):
        print(f'tc0102,{b}')
