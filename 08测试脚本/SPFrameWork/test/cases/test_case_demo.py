# """
# 测试用例模块演示
# """
# from test.page.rob_tenants import RTMy
# from utils.base import Decorator
# from GUI.keywords import KeyWords
# from utils.assertutil import CustomAssert as cassert
# from utils.fileutil import ParserUtils
# from config.config import DATA_PATH
# import os
# class TestRTMy:
#
#     info_dict_list = ParserUtils(os.path.join(DATA_PATH,'ui_case.xlsx')).parse_test_info_dict('rob_tenants')
#     # print(info_dict_list)
#     # [{},{},{}]
#     @Decorator.casedata(info_dict_list)
#     def test_change_info(self, info_dict):
#         test_data = info_dict['test_data']
#         RTMy().change_info(**test_data)
#         actual = KeyWords().get_ele_txt(RTMy.info_name)
#         cassert.assert_contain('v1.0', actual, info_dict, 'test')
#         # KeyWords().quit()
#
# '''
#     dict1 = [{'uname': 'test01', 'uphone': '18800000001', 'unickname': '字数补丁', 'usex': '男'}]
#
#     @Decorator.casedata(dict1)
#     def test_change_info1(self, test_data):
#         RTMy().change_info(**test_data)
#         actual = KeyWords().get_ele_txt(RTMy.info_name)
#         info_dict = {'expect': 'pass1', 'case_id': 'test001', 'module_name': 'login', 'case_desc': 'dev',
#                      'test_data': 'dyz',
#                      'test_type': '功能测试', 'api_url': '172.16.3.124', 'request_method': 'pass', }
#         cassert.assert_contain('v1.0', actual, info_dict, 'test')
#         KeyWords().quit()
# '''
# if __name__ == '__main__':
#
#
#     pass
