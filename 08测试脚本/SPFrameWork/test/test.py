"""
Test
"""
class Test:


    def __init__(self):
        pass

    @classmethod
    def isnot(self,l1,l2):
        flag = True
        mylist = []
        for item in l2:
            if item in l1:
                mylist.append(1)
            else:
                mylist.append(0)
        for l in mylist:
            flag = l and flag
        return flag


if __name__ == '__main__':
    l1 = [1,2,[3,4],{'a':'b'}]
    l2 = [2,{'a':'b'}]
    l3 = []

    print(Test.isnot(l1,l2))

    pass