'''
抢租客页面对象，包含页面元素信息和各功能点方法
'''

from utils.fileutil import ParserUtils
from config.config import PAGE_PATH
from test.page.base_page import BasePage
import time



_login_acc = {'uname': 'pt', 'upass': '123', 'imgcode': '0000'}

class PT1(BasePage):
    '''
    抢租客角色我的类，RT即RobTenants
    '''
    # first_navigation_loc = ('PARTIAL_LINK_TEXT', '人事管理')
    # second_navigation_loc = ('PARTIAL_LINK_TEXT', '员工信息')


    home_page = ParserUtils(PAGE_PATH + '/rob_pt.cnf').parse_dict_by_tuple('home')
    # print(home_page)
    navigation_vehicle_related = eval(home_page['vehicle_related'])

    my_page = ParserUtils(PAGE_PATH + '/rob_pt.cnf').parse_dict_by_tuple('vehicle_related')
    parking_space_management = eval(my_page['parking_space_management'])
    loc_iframe = eval(my_page['my_iframe'])
    id = eval(my_page['id'])
    revise = eval(my_page['revise'])
    seatid = eval(my_page['seatid'])
    sizeid = eval(my_page['sizeid'])
    address = eval(my_page['address'])
    modify = eval(my_page['modify'])
    info_name = eval(my_page['info_name'])


    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.navigation_vehicle_related)

    def change_info(self, **kwargs):
        kw = self.kw
        kw.click(self.parking_space_management)
        kw.switch_iframe(self.loc_iframe)
        kw.click(self.id)
        kw.click(self.revise)
        kw.input(self.seatid, kwargs['seatid'])
        kw.input(self.sizeid, kwargs['sizeid'])
        kw.input(self.address, kwargs['address'])
        kw.click(self.modify)

class PT2(BasePage):
    '''
    抢租客角色我的类，RT即RobTenants
    '''
    # first_navigation_loc = ('PARTIAL_LINK_TEXT', '人事管理')
    # second_navigation_loc = ('PARTIAL_LINK_TEXT', '员工信息')


    home_page = ParserUtils(PAGE_PATH + '/rob_pt.cnf').parse_dict_by_tuple('home')
    # print(home_page)
    navigation_vehicle_related = eval(home_page['vehicle_related'])

    my_page = ParserUtils(PAGE_PATH + '/rob_pt.cnf').parse_dict_by_tuple('vehicle_related_place')
    place = eval(my_page['place'])
    loc_iframe = eval(my_page['my_iframe'])
    id = eval(my_page['id'])
    revise = eval(my_page['revise'])
    site = eval(my_page['site'])
    modify = eval(my_page['modify'])
    info_name = eval(my_page['info_name'])


    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.navigation_vehicle_related)

    def change_info(self, **kwargs):
        kw = self.kw
        kw.click(self.place)
        kw.switch_iframe(self.loc_iframe)
        kw.click(self.id)
        kw.click(self.revise)
        kw.input(self.site, kwargs['site'])
        kw.click(self.modify)


class PT3(BasePage):
    '''
    抢租客角色我的类，RT即RobTenants
    '''
    # first_navigation_loc = ('PARTIAL_LINK_TEXT', '人事管理')
    # second_navigation_loc = ('PARTIAL_LINK_TEXT', '员工信息')


    home_page = ParserUtils(PAGE_PATH + '/rob_pt.cnf').parse_dict_by_tuple('home')
    # print(home_page)
    navigation_order_management = eval(home_page['order_management'])


    my_page = ParserUtils(PAGE_PATH + '/rob_pt.cnf').parse_dict_by_tuple('view_order')
    view_order = eval(my_page['view_order'])
    loc_iframe = eval(my_page['my_iframe'])
    id = eval(my_page['id'])
    revise = eval(my_page['revise'])
    order_status = eval(my_page['order_status'])
    modify = eval(my_page['modify'])
    info_name = eval(my_page['info_name'])


    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.navigation_order_management)

    def change_info(self, **kwargs):
        kw = self.kw
        kw.click(self.view_order)
        kw.switch_iframe(self.loc_iframe)
        kw.click(self.id)
        kw.click(self.revise)
        kw.input(self.order_status, kwargs['order_status'])
        kw.click(self.modify)

class PtUser(BasePage):
    '''
    抢租客角色我的类，RT即RobTenants
    '''
    # first_navigation_loc = ('PARTIAL_LINK_TEXT', '人事管理')
    # second_navigation_loc = ('PARTIAL_LINK_TEXT', '员工信息')

    user_management= ParserUtils(PAGE_PATH + '/platform.cnf').parse_dict_by_tuple('user_management')
    user_management_page = eval(user_management['user_management'])
    lessor = eval(user_management['lessor'])
    ifrmae1 = eval(user_management['iframe1'])
    increase_lessor = eval(user_management['increase_lessor'])
    user=eval(user_management['user'])
    lessor_name = eval(user_management['lessor_name'])
    lessor_nickname = eval(user_management['lessor_nickname'])
    lessor_phone = eval(user_management['lessor_phone'])
    lessor_password = eval(user_management['lessor_password'])
    lessor_sex = eval(user_management['lessor_sex'])
    increase_lessor_button = eval(user_management['increase_lessor_button'])
    information = eval(user_management['information'])

    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.user_management_page)
        self.kw.click(self.lessor)

    def insert_lessor(self, **kwargs):
        kw = self.kw
        # kw.switch_iframe_and_execute(self.loc_iframe,'click', self.loc_change_info)
        # kw.switch_iframe_and_execute(self.loc_iframe,self.loc_change_info, 'click')
        # kw.switch_iframe_and_execute(self.loc_iframe, self.loc_change_info)
        kw.switch_iframe(self.ifrmae1)
        # print('111111111111')
        # time.sleep(2)
        kw.click(self.increase_lessor)
        kw.click(self.user)
        kw.input(self.lessor_name, kwargs['lessor_name'])
        kw.input(self.lessor_nickname, kwargs['lessor_nickname'])
        kw.input(self.lessor_phone, kwargs['lessor_phone'])
        kw.input(self.lessor_password, kwargs['lessor_password'])
        kw.input(self.lessor_sex,kwargs['lessor_sex'])
        kw.click(self.lessor_sex)
        kw.click(self.increase_lessor_button)


# if __name__ == '__main__':
#     KW = {'lessor_name':'出租方9576',
#             'lessor_nickname':'s11总冠军低调',
# 'lessor_phone':'13254565548',
# 'lessor_password':'123456',
# 'lessor_sex':'男'}
#     PtUser().insert_lessor()
    # pass
