'''
抢租客页面对象，包含页面元素信息和各功能点方法
'''

from utils.fileutil import ParserUtils
from config.config import PAGE_PATH
from test.page.base_page import BasePage




_login_acc = {'uname': '出租方1', 'upass': '123', 'imgcode': '0000'}

class PT(BasePage):


    home_page = ParserUtils(PAGE_PATH + '/rob_lessor.cnf').parse_dict_by_tuple('home')
    navigation_homepage = eval(home_page['homepage'])

    my_page = ParserUtils(PAGE_PATH + '/rob_lessor.cnf').parse_dict_by_tuple('homepage')
    homepage = eval(my_page['homepage'])
    loc_iframe = eval(my_page['my_iframe'])
    add_paking = eval(my_page['add_paking'])
    car_id = eval(my_page['car_id'])
    size_id = eval(my_page['size_id'])
    seat_id = eval(my_page['seat_id'])
    address = eval(my_page['address'])
    add = eval(my_page['add'])
    info_name = eval(my_page['info_name'])


    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.navigation_homepage)

    def change_info(self, **kwargs):
        kw = self.kw
        kw.click(self.homepage)
        kw.switch_iframe(self.loc_iframe)
        self.driver.execute_script('document.body.scrollTop=document.documentElement.scrollTop=0')
        kw.click(self.add_paking)
        kw.input(self.car_id, kwargs['car_id'])
        kw.input(self.size_id, kwargs['size_id'])
        kw.input(self.seat_id, kwargs['seat_id'])
        kw.input(self.address, kwargs['address'])
        kw.click(self.add)

class Rent_out(BasePage):
    home_page = ParserUtils(PAGE_PATH + '/rob_lessor.cnf').parse_dict_by_tuple('home')
    navigation_homepage = eval(home_page['homepage'])

    my_page = ParserUtils(PAGE_PATH + '/rob_lessor.cnf').parse_dict_by_tuple('stop')
    ifram_a= eval(my_page['ifram'])
    update_a = eval(my_page['update'])   #修改
    carport = eval(my_page['carport'])
    id_a = eval(my_page['id'])
    site_a = eval(my_page['site'])
    detailed_address_a = eval(my_page['detailed_address'])
    updata_b = eval(my_page['update_b'])
    close_a = eval(my_page['close'])
    mymodallabel_a=eval(my_page['mymodallabel'])


    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.navigation_homepage)

    def change_info(self, **kwargs):
        kw = self.kw
        kw.switch_iframe(self.ifram_a)
        kw.click(self.update_a)
        kw.input(self.carport, kwargs['carport'])
        kw.input(self.id_a, kwargs['id'])

        kw.input(self.site_a, kwargs['site'])
        kw.input(self.detailed_address_a, kwargs['detailed_address'])
        kw.click(self.updata_b)

class My(BasePage):
    home_page = ParserUtils(PAGE_PATH + '/rob_lessor.cnf').parse_dict_by_tuple('home')
    navigation_my = eval(home_page['my'])
    my_page = ParserUtils(PAGE_PATH + '/rob_lessor.cnf').parse_dict_by_tuple('my_change')
      #主页
    ifram_a= eval(my_page['ifram'])
    change_a=eval(my_page['change'])
    user_names = eval(my_page['user_name'])
    user_phos=eval(my_page['user_pho'])
    user_nicknames = eval(my_page['user_nickname'])
    user_sexs = eval(my_page['user_sex'])
    put_ins = eval(my_page['put_in'])
    lessors_a = eval(my_page['lessors'])



    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.navigation_my)
        # self.kw.click(self.navigation_loc2)

    def change_info(self, **kwargs):
        kw = self.kw
        kw.switch_iframe(self.ifram_a)
        kw.click(self.change_a)
        kw.input(self.user_names, kwargs['user_name'])
        kw.input(self.user_phos, kwargs['user_pho'])
        kw.input(self.user_nicknames, kwargs['user_name'])
        kw.input(self.user_sexs, kwargs['user_sex'])
        kw.click(self.put_ins)

if __name__ == '__main__':
    KW = {}
    Rent_out().change_info()
    # pass
