'''
登录页面对象，包含页面元素信息和各功能点方法
'''
# import re

from GUI.keywords import KeyWords
from config.config import Config


class LoginPage(KeyWords):
    '''
    登录页面类
    '''

    # url = f(BASE_PATH+'/config/woniuboss.cnf').get_ini_option_value('base','url')
    # loc_acc = f(BASE_PATH+'/config/woniuboss.cnf').get_ini_option_value('login','uname')
    # loc_pwd = f(BASE_PATH+'/config/woniuboss.cnf').get_ini_option_value('login','upass')
    # loc_vfcode = f(BASE_PATH + '/config/woniuboss.cnf').get_ini_option_value('login', 'vfcode')
    # loc_login_button = f(BASE_PATH + '/config/woniuboss.cnf').get_ini_option_value('login', 'login_button')

    # c = Config().get('login', index=1)
    # url = Config().get('url', index=1)
    # loc_acc = c.get('uname')
    # loc_pwd = c.get('upass')
    # loc_vfcode = c.get('vfcode')
    # loc_login_button = c.get('login_button')


    def __init__(self):
        # super(LoginPage, self).__init__()
        super().__init__()
        self.kw = KeyWords()
        c = Config().get('login', index=1)
        self.url = Config().get('url', index=1)
        self.loc_acc = c.get('uname')
        self.loc_pwd = c.get('upass')
        self.loc_vfcode = c.get('vfcode')
        self.loc_login_button = c.get('login_button')

    def login(self, **kwargs):
        kw = self.kw
        kw.open(self.url)
        kw.input(eval(self.loc_acc), kwargs['acc'])
        kw.input(eval(self.loc_pwd), kwargs['pwd'])
        kw.input(eval(self.loc_vfcode), kwargs['vfc'])
        kw.click(eval(self.loc_login_button))
        kw.quit()

    def decrypt(self):
        driver = self.driver
        driver.find_element_by_id('btn-decrypt').click()
        secondPass = driver.find_element_by_name('secondPass')
        secondPass.click()
        secondPass.clear()
        secondPass.send_keys('woniu123')
        driver.find_element_by_css_selector('button.btn-info:nth-child(1)').click()


    def logout(self):
        self.driver.find_element_by_partial_link_text(u'注销').click()


if __name__ == '__main__':
    di = {'acc':'WNCD000','pwd':'woniu123','vfc':'0000'}
    LoginPage().login(**di)
    # r = LoginPage().loc_acc
    # print(r,type(r))
    # msg = '第 1 条到等 6 行数据，总共 11 条'
    # r = re.findall(r'\d+', msg)
    # print(r[2])
    # r = Config().get('url', index=1)
    # print(r,type(r))
    pass
