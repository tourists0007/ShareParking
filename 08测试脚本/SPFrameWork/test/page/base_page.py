'''
基础页面对象，包含页面元素信息和各功能点方法，eg：登录
'''
# import re

from GUI.keywords import KeyWords
from config.config import Config,CONFIG_PATH
from utils.fileutil import ParserUtils,FileUtils

class BasePage(KeyWords):
    '''
    基础页面类
    '''
    # url = f(BASE_PATH+'/config/woniuboss.cnf').get_ini_option_value('base','url')
    # loc_acc = f(BASE_PATH+'/config/woniuboss.cnf').get_ini_option_value('login','uname')
    # loc_pwd = f(BASE_PATH+'/config/woniuboss.cnf').get_ini_option_value('login','upass')
    # loc_vfcode = f(BASE_PATH + '/config/woniuboss.cnf').get_ini_option_value('login', 'vfcode')
    # loc_login_button = f(BASE_PATH + '/config/woniuboss.cnf').get_ini_option_value('login', 'login_button')

    # c = Config().get('login', index=1)
    # url = Config().get('url', index=1)
    # loc_acc = c.get('uname')
    # loc_pwd = c.get('upass')
    # loc_vfcode = c.get('vfcode')
    # loc_login_button = c.get('login_button')

    # 登录元素

    # 登录元素
    url = Config().get('url')
    # 登录界面的所有元素，login_page为字典
    login_page = ParserUtils(CONFIG_PATH+'/SPPage/base.cnf').parse_dict_by_tuple('login')
    loc_uname = eval(login_page['uname'])
    loc_upass = eval(login_page['upass'])
    loc_imgcode = eval(login_page['imgcode'])
    loc_login_button = eval(login_page['login_button'])
    # 导航栏元素
    first_navigation_loc = ('PARTIAL_LINK_TEXT', '人事管理')
    second_navigation_loc = ('PARTIAL_LINK_TEXT', '员工信息')


    def __init__(self):
        # super(LoginPage, self).__init__()
        super().__init__()
        self.kw = KeyWords()
        # c = Config().get('login', index=1)
        # self.loc_acc = c.get('uname')
        # self.loc_pwd = c.get('upass')
         # self.loc_vfcode = c.get('vfcode')
        # self.loc_login_button = c.get('login_button')

    def login(self, **kwargs):
        try:
            kw = self.kw
            kw.open(self.url)
            kw.input(self.loc_uname, kwargs['uname'])
            kw.input(self.loc_upass, kwargs['upass'])
            kw.input(self.loc_imgcode, kwargs['imgcode'])
            kw.click(self.loc_login_button)
        except Exception as e:
            self.logger.error(f'登陆错误'+ str(e))

    def navigation(self, first_loc, second_loc=None, third_loc=None):
        kw = self.kw
        try:
            if kw.is_element_present(second_loc):
                kw.click(second_loc)
            else:
                kw.click(first_loc)
                kw.click(second_loc)
        except Exception as e:
            self.logger.error(f'导航栏错误，{first_loc}或{second_loc}异常'+ str(e))

    def decrypt(self):
        driver = self.driver
        driver.find_element_by_id('btn-decrypt').click()
        secondPass = driver.find_element_by_name('secondPass')
        secondPass.click()
        secondPass.clear()
        secondPass.send_keys('woniu123')
        driver.find_element_by_css_selector('button.btn-info:nth-child(1)').click()


    def logout(self):
        self.driver.find_element_by_partial_link_text(u'注销').click()

if __name__ == '__main__':
    di = {'acc': 'pt', 'pwd': '123', 'vfc': '0000'}
    BasePage().login(**di)
    # r = BasePage.loc_upass
    # print(r,type(r))
    pass