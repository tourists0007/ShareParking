'''
物业页面对象，包含页面元素信息和各功能点方法
'''

from utils.fileutil import ParserUtils
from config.config import PAGE_PATH
from test.page.base_page import BasePage

_login_acc = {'uname': '抢租客0', 'upass': '123', 'imgcode': '0000'}
_login_pro = {'uname': '物业0', 'upass': '123', 'imgcode': '0000'}

class Property(BasePage):
    home_page = ParserUtils(PAGE_PATH + '/property.cnf').parse_dict_by_tuple('home')
    order_manage = eval(home_page['order_manage'])
    users_manage = eval(home_page['users_manage'])
    car = eval(home_page['car'])

    user_page = ParserUtils(PAGE_PATH + '/property.cnf').parse_dict_by_tuple('users')
    property=  eval(user_page['property'])
    pro_iframe = eval(user_page['pro_iframe'])
    pro_add = eval(user_page['pro_add'])
    input_name = eval(user_page['input_name'])
    input_phone= eval(user_page['input_phone'])
    input_text = eval(user_page['input_text'])
    input_ssl = eval(user_page['input_ssl'])
    input_pass= eval(user_page['input_pass'])
    alert_sure = eval(user_page['alert_sure'])
    alert_info = eval(user_page['alert_info'])

    choose_info = eval(user_page['choose_info'])
    change_pro = eval(user_page['change_pro'])
    change_sure = eval(user_page['change_sure'])


    def __init__(self):
        super().__init__()
        self.login(**_login_pro)


    def add_pro(self, **kwargs):
        kw = self.kw
        kw.click(self.property)
        kw.switch_iframe(self.pro_iframe)

        kw.click(self.pro_add)
        kw.input(self.input_name, kwargs['uname'])
        kw.input(self.input_phone, kwargs['uphone'])
        kw.input(self.input_text, kwargs['uinfo'])
        kw.input(self.input_ssl, kwargs['ussl'])
        kw.input(self.input_pass, kwargs['upass'])
        kw.click(self.alert_sure)

    def change_info(self,**kwargs):
        kw = self.kw
        kw.click(self.property)
        kw.switch_iframe(self.pro_iframe)

        kw.click(self.choose_info)
        kw.click(self.change_pro)
        kw.input(self.input_text, kwargs['uinfo'])
        kw.input(self.input_ssl, kwargs['ussl'])
        kw.click(self.change_sure)
