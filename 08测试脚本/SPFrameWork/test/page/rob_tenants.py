'''
抢租客页面对象，包含页面元素信息和各功能点方法
'''

from utils.fileutil import ParserUtils
from config.config import PAGE_PATH
from test.page.base_page import BasePage

_login_acc = {'uname': '抢租客0', 'upass': '123', 'imgcode': '0000'}

class RTMy(BasePage):
    '''
    抢租客角色我的类，RT即RobTenants
    '''


    home_page = ParserUtils(PAGE_PATH + '/rob_tenants.cnf').parse_dict_by_tuple('home')
    navigation_my = eval(home_page['my'])
    navigation_order_history = eval(home_page['order_history'])
    navigation_home = eval(home_page['home'])

    navigation_suggest = eval(home_page['suggest'])
    navigation_dropdown = eval(home_page['dropdown'])


    my_page = ParserUtils(PAGE_PATH + '/rob_tenants.cnf').parse_dict_by_tuple('my')
    info_name = eval(my_page['info_name'])
    loc_iframe = eval(my_page['my_iframe'])
    loc_change_info = eval(my_page['change_info'])
    uname = eval(my_page['uname'])
    uphone = eval(my_page['uphone'])
    unickname = eval(my_page['unickname'])
    usex = eval(my_page['usex'])
    uclose = eval(my_page['uclose'])
    usubmit = eval(my_page['usubmit'])
    realname = eval(my_page['realname'])

    def __init__(self):
        super().__init__()
        self.login(**_login_acc)
        self.kw.click(self.navigation_my)
        # self.kw.click(self.navigation_loc2)

    def change_info(self, **kwargs):
        kw = self.kw
        # kw.switch_iframe_and_execute(self.loc_iframe,'click', self.loc_change_info)
        # kw.switch_iframe_and_execute(self.loc_iframe,self.loc_change_info, 'click')
        # kw.switch_iframe_and_execute(self.loc_iframe, self.loc_change_info)
        kw.switch_iframe(self.loc_iframe)
        kw.click(self.loc_change_info)
        kw.input(self.uname, kwargs['uname'])
        kw.input(self.uphone, kwargs['uphone'])
        kw.input(self.unickname, kwargs['unickname'])
        kw.input(self.usex, kwargs['usex'])
        kw.click(self.usubmit)


class BuyCarnum(BasePage):
    '''
      抢租客角色主页下单以及查看订单
      '''

    home_page = ParserUtils(PAGE_PATH + '/rob_tenants.cnf').parse_dict_by_tuple('home')
    navigation_my = eval(home_page['my'])
    navigation_order_history = eval(home_page['order_history'])
    navigation_home = eval(home_page['home'])

    navigation_suggest = eval(home_page['suggest'])
    navigation_dropdown = eval(home_page['dropdown'])

    my_page = ParserUtils(PAGE_PATH + '/rob_tenants.cnf').parse_dict_by_tuple('main')
    buy_iframe = eval(my_page['buy_iframe'])
    buy_map = eval(my_page['buy_map'])
    buy_car = eval(my_page['buy_car'])
    buy_sure = eval(my_page['buy_sure'])

    history = ParserUtils(PAGE_PATH + '/rob_tenants.cnf').parse_dict_by_tuple('order_history')
    history_iframe = eval(history['history_iframe'])
    end_age = eval(history['end_age'])
    info_order=eval(history['info_order'])

    def __init__(self):
        super().__init__()
        self.login(**_login_acc)


    def buy_carnum(self):
        kw = self.kw
        kw.switch_iframe(self.buy_iframe)
        kw.click(self.buy_map)
        kw.click(self.buy_car)
        kw.click(self.buy_sure)

    def query_order(self):
        kw = self.kw
        self.kw.click(self.navigation_order_history)
        kw.switch_iframe(self.history_iframe)
        kw.click(self.end_age)



if __name__ == '__main__':
    # BuyCarnum().buy_carnum()

    pass
