/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9696212093960215, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.98825, 500, 1500, "get_parkingid"], "isController": false}, {"data": [0.999, 500, 1500, "get_抢购"], "isController": false}, {"data": [0.9895, 500, 1500, "get_pagecount"], "isController": false}, {"data": [0.9865, 500, 1500, "get添加车位"], "isController": false}, {"data": [0.975, 500, 1500, "抢购"], "isController": false}, {"data": [0.997904662126768, 500, 1500, "平台完成订单"], "isController": false}, {"data": [0.8687179487179487, 500, 1500, "平台方审核订单"], "isController": true}, {"data": [0.89475, 500, 1500, "平台方审核车位"], "isController": true}, {"data": [0.99675, 500, 1500, "出租方——添加车位"], "isController": false}, {"data": [0.996155817529472, 500, 1500, "get平台"], "isController": false}, {"data": [0.997, 500, 1500, "平台——审核车位"], "isController": false}, {"data": [0.976, 500, 1500, "出租方添加车位"], "isController": true}, {"data": [0.97175, 500, 1500, "抢租客抢购车位"], "isController": true}, {"data": [0.9366515837104072, 500, 1500, "get_pagecount 订单管理"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 19849, 0, 0.0, 116.00015114111534, 3, 3184, 61.0, 309.0, 403.0, 647.0, 27.733881052683056, 319.6894195720593, 7.035104699365651], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["get_parkingid", 2000, 0, 0.0, 150.80850000000018, 6, 1887, 106.0, 315.8000000000002, 388.9499999999998, 668.7800000000002, 2.8909212064970564, 45.05082589553511, 0.6944986492170663], "isController": false}, {"data": ["get_抢购", 2000, 0, 0.0, 12.643000000000024, 3, 2004, 6.0, 14.0, 22.0, 114.97000000000003, 2.8920164440055007, 1.3132691859985917, 0.5846166053800183], "isController": false}, {"data": ["get_pagecount", 2000, 0, 0.0, 166.3380000000001, 9, 1700, 124.0, 362.0, 417.0, 585.9000000000001, 2.8900021819516475, 64.04007764713361, 0.708389206708851], "isController": false}, {"data": ["get添加车位", 2000, 0, 0.0, 168.8634999999998, 13, 3184, 123.5, 362.0, 424.0, 672.7900000000002, 2.890607837015968, 64.05349846653255, 0.6661947749372739], "isController": false}, {"data": ["抢购", 2000, 0, 0.0, 208.37649999999988, 54, 1805, 161.0, 389.0, 497.9499999999998, 710.0, 2.8934682846941313, 0.45210441948345803, 0.0], "isController": false}, {"data": ["平台完成订单", 1909, 0, 0.0, 30.65793609219482, 6, 1978, 12.0, 56.0, 112.0, 347.9000000000001, 2.817080153234994, 0.3466329094800871, 1.0316455639288307], "isController": false}, {"data": ["平台方审核订单", 1950, 0, 0.0, 381.322564102564, 38, 3201, 311.0, 722.7000000000003, 835.8999999999996, 1109.96, 2.8213519918745065, 153.50972090272774, 2.311952990036287], "isController": true}, {"data": ["平台方审核车位", 2000, 0, 0.0, 351.3549999999999, 28, 2888, 289.0, 660.0, 767.7999999999993, 1203.3300000000006, 2.890123292659665, 109.43677356299459, 2.3933833517337852], "isController": true}, {"data": ["出租方——添加车位", 2000, 0, 0.0, 35.21250000000003, 6, 1868, 13.0, 76.90000000000009, 136.0, 337.99, 2.8904114934322624, 0.35565610173092294, 1.309717707961494], "isController": false}, {"data": ["get平台", 1951, 0, 0.0, 106.43926191696552, 13, 1808, 78.0, 209.0, 265.39999999999986, 470.48, 2.8239673919336723, 69.44377506343433, 0.6425628928911579], "isController": false}, {"data": ["平台——审核车位", 2000, 0, 0.0, 34.20849999999993, 6, 2218, 12.0, 61.90000000000009, 132.0, 314.97, 2.8913600379346436, 0.35577281716773934, 0.9910814192529882], "isController": false}, {"data": ["出租方添加车位", 2000, 0, 0.0, 204.0759999999997, 22, 3258, 153.0, 402.0, 489.6999999999989, 1063.5200000000004, 2.8904156706776, 64.40489683383868, 1.9758700873772657], "isController": true}, {"data": ["抢租客抢购车位", 2000, 0, 0.0, 221.01950000000028, 58, 3251, 171.0, 405.9000000000001, 511.9499999999998, 726.94, 2.8911635921551166, 1.7646262159149881, 0.584444202711044], "isController": true}, {"data": ["get_pagecount 订单管理", 1989, 0, 0.0, 243.03519356460495, 16, 1994, 178.0, 529.0, 619.0, 829.1999999999989, 2.8781329902919084, 85.47830127027102, 0.6717517428513341], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 19849, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
